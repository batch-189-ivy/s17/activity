console.log('Hello World')

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function enterUserName(){
	let UserName = prompt('Enter your username')
	console.log('Hello, ' + UserName)
	}

	enterUserName()

	let userAge = prompt('Enter your age')
	console.log('You are ' + userAge + 'years old.')

	let userLocation = prompt('Enter your location')
	console.log('You live in ' + userLocation)


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here: "one", "\n", "two"

	function sayTop5Bands(){
	let top5 = alert('Top 5 Favorite Bands/Musical Artists:')
	}

	sayTop5Bands()

	console.log("\n", "1.Sugarfree", "\n", "2.Pomplamoose", "\n", "3.Orange&Lemons", "\n", "4.Rivermaya", "\n", "5.Eraserheads")
	

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function sayTop5Movies(){
	let top5 = alert('Top 5 Favorite Movies/Series:')
	}

	sayTop5Movies()

	console.log('1. Train To Busan', '\n', 'Rotten Tomatoes Rating: 94%')
	console.log('2. Hometown Cha-Cha-Cha', '\n', 'Rotten Tomatoes Rating: 97%')
	console.log('3. My Neighbor Totoro', '\n', 'Rotten Tomatoes Rating: 95%')
	console.log('4. Squid Game', '\n', 'Rotten Tomatoes Rating: 95%')
	console.log('5. Spy X Family', '\n', 'Rotten Tomatoes Rating: 89%')


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


/*console.log(friend1);
console.log(friend2);
*/
printFriends();